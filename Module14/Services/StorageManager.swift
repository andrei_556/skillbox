

import RealmSwift

let realm = try! Realm()

class StorageManager {
    
    static func save(task: ModelRealm) {
        
        try! realm.write {
            realm.add(task)
        }
    }
}
