

import UIKit
import RealmSwift


class RealmTableViewController: UITableViewController {
    
    var tasksList: Results<ModelRealm>!
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tasksList = realm.objects(ModelRealm.self)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tasksList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = tasksList[indexPath.row].task
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func addTask(_ sender: Any) {
        
    let alert = UIAlertController(title: "Новая задача", message: "Введите новую задачу", preferredStyle: .alert)
    let saveAction = UIAlertAction(title: "Сохранить", style: .default) { _ in
        guard let task = alert.textFields?.first?.text, !task.isEmpty else { return }
        let newTask = ModelRealm()
        newTask.task = task
        StorageManager.save(task: newTask)
        self.tableView.insertRows(at: [IndexPath(row: self.tasksList.count - 1, section: 0)], with: .automatic)
    }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .destructive)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let task = tasksList[indexPath.row]
        
        try! self.realm.write {
            self.realm.delete(task)
            tableView.reloadData()
        }
    }
    
}
