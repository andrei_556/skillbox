

import Foundation
import CoreData

struct WeatherForCoreData {

    let condition: String
    let cloud: String
    let wind: String
    let windSpeed: Double
    let temp: Double
    let humidity: String
}
